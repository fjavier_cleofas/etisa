<section>
    <div class="seccion_info">
        <div class="container">
            <div class="row div-social-icons">
              <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="container">
                      <img src="{{asset('img/logo/logo_etisa_color.png')}}" class="logo_etisa_color img-responsive">
                  </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6">
                      <div class="text-justify">
                          <p class="texto_info">{{__('auth.text_us_section1')}}</p>
                          <p class="texto_info">{{__('auth.text_us_section2')}}</p>
                      </div>
              </div>
            </div>

            <div class="row div-social-icons">
                <div class="col-xs-12 col-sm-8">
                    <div class="text_quality">
                        <p class="texto_carousel_titulo">{{__('auth.text_carousel_us1')}}</p>
                        <hr id="line_horizontal_quality">
                        <p class="texto_carousel">{{__('auth.text_carousel_us2')}}</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <img src="{{asset('img/carousel/quality-calidad.png')}}" class="img_carousel img-responsive slide">
                </div>
            </div>

<!-- aplicar flex direction -->
            <div class="row div-social-icons hidden-xs">
              <div class="col-xs-12 col-sm-4">
                  <img src="{{asset('img/carousel/environment.png')}}" class="img_carousel img-responsive slide">
              </div>
              <div class="col-xs-12 col-sm-8">
                  <div class="text_quality">
                      <p class="texto_carousel_titulo">{{__('auth.text_carousel_us3')}}</p>
                      <hr id="line_horizontal_environment">
                      <p class="texto_carousel">{{__('auth.text_carousel_us4')}}</p>
                  </div>
              </div>
            </div>

            <div class="row div-social-icons visible-xs">
              <div class="col-xs-12">
                  <div class="text_quality">
                      <p class="texto_carousel_titulo">{{__('auth.text_carousel_us3')}}</p>
                      <hr id="line_horizontal_environment">
                      <p class="texto_carousel">{{__('auth.text_carousel_us4')}}</p>
                  </div>
              </div>
              <div class="col-xs-12">
                  <img src="{{asset('img/carousel/environment.png')}}" class="img_carousel img-responsive slide">
              </div>
            </div>
<!-- aplicar flex direction fin -->

            <div class="row div-social-icons">
                <div class="col-xs-12 col-sm-8">
                    <div class="text_quality">
                        <p class="texto_carousel_titulo">{{__('auth.text_carousel_us5')}}</p>
                        <hr id="line_horizontal_health">
                        <p class="texto_carousel">{{__('auth.text_carousel_us6')}}</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <img src="{{asset('img/carousel/health.png')}}" class="img_carousel img-responsive slide">
                </div>
            </div>



<!-- aplicar flex direction -->
            <div class="row div-social-icons hidden-xs">
              <div class="col-sm-4">
                    <img src="{{asset('img/carousel/safety.png')}}" class="img_carousel img-responsive slide">
                </div>
                <div class="col-sm-8">
                    <div class="text_quality">
                        <p class="texto_carousel_titulo">{{__('auth.text_carousel_us7')}}</p>
                        <hr id="line_horizontal_safety">
                        <p class="texto_carousel">{{__('auth.text_carousel_us8')}}</p>
                    </div>
                </div>
            </div>

            <div class="row div-social-icons visible-xs">
              <div class="col-xs-12">
                    <div class="text_quality">
                        <p class="texto_carousel_titulo">{{__('auth.text_carousel_us7')}}</p>
                        <hr id="line_horizontal_safety">
                        <p class="texto_carousel">{{__('auth.text_carousel_us8')}}</p>
                    </div>
                </div>
              <div class="col-xs-12">
                    <img src="{{asset('img/carousel/safety.png')}}" class="img_carousel img-responsive slide">
                </div>
            </div>

<!-- aplicar flex direction fin -->

            <div class="row div-social-icons">
               <div class="col-xs-12 col-sm-8">
                    <div class="text-left">
                        <p class="texto_carousel_titulo">{{__('auth.text_carousel_us9')}}</p>
                        <hr id="nuestra">
                        <p class="texto_info">{{__('auth.text_carousel_us10')}}</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <img src="{{asset('img/us/mission.png')}}" class="img_carousel img-responsive slide">
                </div>
            </div>


<!-- aplicar flex direction  -->

            <div class="row div-social-icons hidden-xs">
                <div class="col-sm-4">
                    <img src="{{asset('img/us/view.png')}}" class="img_carousel img-responsive slide">
                </div>
                <div class="col-sm-8">
                    <div class="text-left">
                        <p class="texto_carousel_titulo">{{__('auth.text_carousel_us11')}}</p>
                        <hr id="nuestra">
                        <p class="texto_info">{{__('auth.text_carousel_us12')}}</p>
                        <p class="texto_info">{{__('auth.text_carousel_us13')}}</p>
                    </div>
                </div>
            </div>

            <div class="row div-social-icons visible-xs">
                <div class="col-xs-12">
                    <div class="text-left">
                        <p class="texto_carousel_titulo">{{__('auth.text_carousel_us11')}}</p>
                        <hr id="nuestra">
                        <p class="texto_info">{{__('auth.text_carousel_us12')}}</p>
                        <p class="texto_info">{{__('auth.text_carousel_us13')}}</p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <img src="{{asset('img/us/view.png')}}" class="img_carousel img-responsive slide">
                </div>
            </div>

<!-- aplicar flex direction fin -->


             <!-- <div class="row div-social-icons">
                <div class="col-xs-8">
                    <div class="text-left">
                        <p class="texto_carousel_titulo">{{__('auth.text_carousel_us14')}}</p>
                        <hr id="nuestra">
                        <p class="texto_info">{{__('auth.text_carousel_us15')}}</p>
                    </div>
                </div>
                <div class="col-xs-4">
                    <img src="{{asset('img/us/values.png')}}" class="img_carousel img-responsive slide">
                </div>
            </div> -->

        </div>
    </div>
</section>