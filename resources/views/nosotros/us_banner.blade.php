<div class="div-us-banner img-fluid">
    @include('template/layouts/menu')
    <div class="text_us_banner">
        <div class="container">
            <p class="texto_us_banner">{{__('auth.text_us_banner1')}}</p>
            <p class="texto_us_banner">{{__('auth.text_us_banner2')}}</p>
            <p class="texto_us_banner_white">{{__('auth.text_us_banner3')}}</p>
            <p class="texto_us_banner_white">{{__('auth.text_us_banner4')}}</p>
            <p class="texto_us_banner_white">{{__('auth.text_us_banner5')}}</p>
        </div>
    </div>
</div>