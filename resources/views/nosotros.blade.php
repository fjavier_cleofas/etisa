@extends('template/main')
@section('keywords', 'Valores, Cuidado de medio ambiente, Control de energía térmica')
@section('description', 'Reducimos sustancialmente los costos de producción de nuestros clientes, estimulando positivamente su crecimiento.')
@section('content')
    @include('nosotros/us_banner')
    @include('nosotros/section_us')
@endsection