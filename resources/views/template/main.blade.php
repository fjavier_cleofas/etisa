<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>@yield('title','ETISA')</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('css/desing_etisa.css')}}">
    <!-- Meta SEO -->
    <meta name="keywords" content="@yield('keywords','')">
    <meta name="description" content="@yield('description','')">
    <meta name="robots" content="index,follow">
    <meta name="author" content="Erasmo Soto Guerrero | Edgar Perez Perez | José Adrián Martínez Marín">
    <meta name="owner" content="Develovers Company Puebla">
</head>

<body id="top">
    @yield('content')
    @include('template/layouts/footer')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>