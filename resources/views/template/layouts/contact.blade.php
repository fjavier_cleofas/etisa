<?php
  /*Meta
  $title_page = "Contacto | MIDOCONLINE";
  $description = "Contacta o reporta algún problema con el equipo de MIDOCONLINE.";
  $url_Canonical = URL.'/contacto';

  $ruta = htmlspecialchars($_GET["src"]);
  if ($ruta == "app") {
  	$activo = 'app';
  }
  else{
 */
  	$activo = 'c';
  //}
?>

@section('content')


<section class="Contact p_top_40 p_bottom_40">
	<div class="row container items-around">
		<div class="col-xs12 col-sm10 relative">
@if($activo == 'app')
				{!! Form::open(['route' => 'contacto.store','method' => 'POST', 'id' => 'Form', 'class' => 'Contact__form']) !!}

@else
			{!! Form::open(['route' => 'contacto.store','method' => 'POST', 'id' => 'Form', 'class' => 'Contact__form']) !!}
@endif
				<div class="row container items-center t-left">
					<h2 class="contact-p">¿Tienes algo que contarnos?</h2>
					<h2 class="contact-p contact-p-2">Escríbenos</h2>
					<img class="contact-img" src="{{asset('img/contacto/contacto.png')}}" alt="Contactanos">
					@if(Session::has('message'))
			    <div class="col-xs12 alert alert-info message-response">
			      {{Session::get('message')}}
			    </div>
					@endif
					<div class="col-xs12 col-sm6 col-lg6">
						<div class="row items-between">
							<div class="col-xs12 m_bottom_20 p_10">
								{!! Form::label('name', '¿Cuál es tu nombre?', ['class' => 't-caption m_bottom_5 contact-label']) !!}
								{!! Form::text('name', null,['class' => 'contact-input','placeholder' => '', 'required', 'id' => 'name']) !!}
							</div>
						</div>
					</div>
					<div class="col-xs12 col-sm6 col-lg6">
						<div class="row items-between">
							<div class="col-xs12 m_bottom_20 p_10">
								{!! Form::label('category', '¿A qué departamento quieres dirigirte?', ['class' => 't-caption m_bottom_5 contact-label']) !!}
								{!! Form::select('category', array('selec' => 'Selecciona una opción', 'comunication' => 'Comunicación', 'support' => 'Soporte', 'sales' => 'Ventas'),'selec',['class' => 'contact-input','placeholder' => '', 'required', 'id' => 'category']) !!}
							</div>
@if($activo == 'app')
							<div class="col-xs12 m_bottom_20 p_10">
								<p class="t-caption">Si necesitas, puedes adjuntar una captura de pantalla.</p>
							  <label class="btn btn-upload t-center" for="archivo" title="Subir archivo">
					        <input type="file" class="sr-only" id="archivo" name="archivo" accept="pdf">Adjuntar
					      </label>
					      <div id="file_message" class="p_10"></div>
					      <div class="fileName"></div>
					      <progress id="barra_de_progreso" value="0" max="100"></progress>
							</div>
@endif
						</div>
					</div>
					<div class="col-xs12 col-sm6 m_bottom_20 p_10">
						{!! Form::label('email', 'Tu dirección email', ['class' => 't-caption m_bottom_5 contact-label']) !!}
						{!! Form::email('email', null,['class' => 'contact-input','placeholder' => '', 'required', 'id' => 'email']) !!}
					</div>
					<div class="col-xs12 col-sm6 m_bottom_20 p_10">
						{!! Form::label('subject', 'Asunto', ['class' => 't-caption m_bottom_5 contact-label']) !!}
						{!! Form::text('subject', null,['class' => 'contact-input','placeholder' => '', 'required', 'id' => 'subject']) !!}
					</div>
					<div class="col-xs12 m_bottom_20 p_10">
						{!! Form::label('message', 'Mensaje', ['class' => 't-caption m_bottom_5 contact-label']) !!}
						{!! Form::textarea('message', null,['cols' => '30', 'rows' => '10', 'class' => 'contact-input','placeholder' => '', 'required', 'id' => 'message']) !!}
					</div>
@if($activo == 'app')
					<div class="col-xs12 m_bottom_20 p_10">
						<a href="#" class="btn btn-cta btn-mega" id="submit_docs">Enviar reporte</a>
					</div>
@else
					<div class="col-xs12 m_bottom_20 p_10">
						{!! Form::submit('Enviar', ['class' => 'contact-btn btn btn-cta btn-mega']) !!}
					</div>
@endif

				</div>
			{!! Form::close() !!}
			<div id="statusMessage">
		    <div id="sendingMessage" class="statusMessage enviando">
		        <p>Se esta enviando tu mensaje. Por favor espera ...</p>
		    </div>
		    <div id="successMessage" class="statusMessage enviado">
		        <p><strong>Mensaje Enviado.</strong> <br> En breve nos pondremos en contacto contigo <br> Saludos!</p>
		    </div>
		    <div id="failureMessage" class="statusMessage no-enviado">
		        <p>Hubo un problema al enviar tu mensaje. <br> Por favor, vuelve a intentarlo.</p>
		    </div>
		    <div id="incompleteMessage" class="statusMessage incompleto">
		        <p>Por favor, completa todos los campos antes de enviarlo.</p>
		    </div>
			</div>
		</div>
	</div>
</section>

@endsection
