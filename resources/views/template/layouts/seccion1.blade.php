<section class="container seccion1" >
  <div class="rectangulo-6 col-md-6 col-sm-6 col-xs-12 inline-block" >
    <p class="texto_expertos top-expertos">{{__('auth.text_banner6')}}</p>
    <p class="texto_expertos">{{__('auth.text_banner7')}}</p>
    <p class="texto_expertos">{{__('auth.text_banner8')}}</p>
    <p class="texto_parrafo top-parrafo">{{__('auth.text_banner9')}}</p>
    <p class="texto_parrafo">{{__('auth.text_banner10')}}</p>
    <p class="texto_parrafo">{{__('auth.text_banner11')}}</p>
    <p class="texto_parrafo">{{__('auth.text_banner12')}}</p>
    <p class="texto_parrafo">{{__('auth.text_banner13')}}</p>
    <p class="texto_parrafo">{{__('auth.text_banner14')}}</p>
    <p class="texto_parrafo">{{__('auth.text_banner15')}}</p>
  </div>

  <div class="col-md-6 col-sm-6 col-xs-12 inline-block no-padding">
    <img src="{{asset('img/banner/etisa.jpg')}}" class="div-img img-fluid img-responsive">
  </div>
</section>
