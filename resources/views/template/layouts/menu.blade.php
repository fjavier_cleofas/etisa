<header class="Header">
  <nav class="navbar {{-- navbar-fixed-top --}} navBar-top" role="navigation">
    <div class="container-fluid">
      <div class="row">
        <div class="navbar-header col-xs-12 col-sm-5 col-md-3">
            <a href="{{ route('welcome') }}"><img src="{{asset('img/logo/logo_etisa.png')}}" alt="{{__('auth.logo_midoc')}}" class="img-responsive img-logo"></a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar color-hamburguer"></span>
                <span class="icon-bar color-hamburguer"></span>
                <span class="icon-bar color-hamburguer"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse col-sm-6 col-md-8" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav ul-right ">
            <li class="margin-right"><a href="{{ route('welcome') }}" class="header-color-menus">{{__('auth.menu_inicio')}}</a></li>
            <li class="margin-right"><a href="{{ route('nosotros') }}" class="header-color-menus">{{__('auth.menu_nosotros')}}</a></li>
            <li class="margin-right"><a href="{{ route('welcome') }}#servicios" class="header-color-menus">{{__('auth.menu_servicios')}}</a></li>
            <li class="margin-right"><a href="{{ route('welcome') }}#contacto" class="header-color-menus">{{__('auth.menu_contacto')}}</a></li>
          </ul>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </nav>
</header>