<footer class="page-footer font-small blue pt-4 text-center footer-above">

  <div class="container-fluid footer-above seccion1">

    <div class="row">
      <div class="col-xs-12 div-social-icons">
        <a href="https://www.facebook.com/Etisa-178537348900937/" target="_blank"><img src="{{asset('img/redes/facebook.png')}}" class="img-responsive inline" alt="Icono Facebook"></a>
        <a href="#" target="_blank"><img src="{{asset('img/redes/twitter.png')}}" class="img-responsive inline" alt="Icono Twitter"></a>
        <a href="#" target="_blank"><img src="{{asset('img/redes/linkedin.png')}}" class="img-responsive inline" alt="Icono Instagram}"></a>
      </div>

      <div class="col-xs-12 div-social-icons">
        <nav class="navbar">
          <div class="container">
            <ul class="nav navbar-nav col-xs-12">
              <li class="col-xs-4"><a class="menu-font-footer" href="{{ route('nosotros') }}">Nosotros</a></li>
              <li class="col-xs-4"><a class="menu-font-footer" href="{{ route('welcome') }}#servicios">Servicios</a></li>
              <li class="col-xs-4"><a class="menu-font-footer" href="{{ route('welcome') }}#contacto">Contacto</a></li>
            </ul>
          </div>
        </nav>
      </div>

      <div class="col-xs-12">
        <div class="col-xs-6 div-footer-logo">
          <img src="{{asset('img/logo/logo_etisa.png')}}" class="logo-footer img-responsive">
        </div>
        <div class=" col-xs-6 text-left">
          <p class="texto_footer">{{__('auth.text_footer1')}}</p>
          <p class="texto_footer">{{__('auth.text_footer2')}}</p>
          <p class="texto_footer">{{__('auth.text_footer3')}}</p>
          <p class="texto_footer">{{__('auth.text_footer4')}}</p>
          <p class="texto_footer">{{__('auth.text_footer5')}}</p>
        </div>
      </div>

      <br>
      <br>
      <br>

    </div>



     <!--  <div class="">

          <img src="{{asset('img/logo/logo_etisa.png')}}" class="logo-footer img-responsive">
          <div id="line"></div>
          <div class="text-left">
              <p class="texto_footer">{{__('auth.text_footer1')}}</p>
              <p class="texto_footer">{{__('auth.text_footer2')}}</p>
              <p class="texto_footer">{{__('auth.text_footer3')}}</p>
              <p class="texto_footer">{{__('auth.text_footer4')}}</p>
          </div>
      </div> -->


  </div>

</footer>