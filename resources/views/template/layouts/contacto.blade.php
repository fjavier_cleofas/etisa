<section class="container seccion1" >
	<div class="container-texto-contacto col-md-7 col-xs-12 inline-block">
	  <p class="texto_contacto_pr top_contacto">{{__('auth.text_contacto')}}</p>
	  <p class="texto_contacto_pr">{{__('auth.text_contacto1')}}</p>
	  <p class="texto_contacto">{{__('auth.text_contacto2')}}</p>
	  <p class="texto_contacto ">{{__('auth.text_contacto3')}}</p>
	  <p class="texto_contacto">{{__('auth.text_contacto4')}}</p>
	  <p class="texto_contacto">{{__('auth.text_contacto5')}}</p>
	  <p class="texto_contacto ">{{__('auth.text_contacto6')}}</p>
	  <p class="texto_contacto">{{__('auth.text_contacto7')}}</p>
	  <p class="texto_contacto">{{__('auth.text_contacto8')}}</p>
	  <p class="texto_contacto ">{{__('auth.text_contacto9')}}</p>
	</div>

	<div class="form-contacto col-md-5 col-xs-12 inline-block" id="contacto">
		<h1 class="mb-2 text-center">¡Contáctanos!</h1>

		@if(Session::has('message'))
    <div class="alert alert-info message-response">
      {{Session::get('message')}}
    </div>
		@endif

		<div class="col-xs-10 col-xs-offset-1">
			{!! Form::open(['route' => 'contacto.web','method' => 'POST']) !!}
				<div class="form-group">
					{!! Form::text('name', null,['class' => 'form-control','placeholder' => 'Nombre completo', 'required']) !!}
				</div>
				<div class="form-group">
					{!! Form::email('email', null,['class' => 'form-control', 'placeholder' => 'Correo electrónico', 'required']) !!}
				</div>
				<div class="form-group">
					{!! Form::text('subject', null,['class' => 'form-control', 'placeholder' => 'Asunto','required']) !!}
				</div>
				<div class="form-group">
					{!! Form::textarea('message', null,['class' => 'form-control bold input-empresas', 'placeholder' => 'Mensaje', 'required']) !!}
				</div>
				<div class="form-group">
					{!! Form::submit('ENVIAR', ['class' => 'btn btn-primary btn-enviar']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	 </div> <!-- /container -->
</section>
