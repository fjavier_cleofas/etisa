<script js>

  // Initialize tooltip component
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// Initialize popover component
$(function () {
  $('[data-toggle="popover"]').popover()
})
</script>

<section class="rectangulo-carrusel container-fluid col-md-12 col-xs-6 col-sm-6">
  <p class="texto_sec3">{{__('auth.text_sec3')}}</p>


  <!-- Carousel container -->
<div id="my-pics" class="div-logos hidden-xs" >
  <div class="r" >

      <div class="col-xs-4">
        <img class="img-logos-size" src="{{asset('img/Carrusel/vw.png')}}" alt="">
      </div>
      <div class="col-xs-4">
        <img class="img-logos-size" src="{{asset('img/Carrusel/bimbo.png')}}" alt="">
      </div>
      <div class="col-xs-4">
        <img class="img-logos-size" src="{{asset('img/Carrusel/alen.png')}}" alt="">
      </div>

      <div class="col-xs-4">
        <img class="img-logos-size" src="{{asset('img/Carrusel/cocacola.png')}}" alt="">
      </div>
      <div class="col-xs-4">
        <img class="img-logos-size" src="{{asset('img/Carrusel/pepsi.png')}}" alt="">
      </div>
      <div class="col-xs-4">
        <img class="img-logos-size" src="{{asset('img/Carrusel/cryoinfra.png')}}" alt="">
      </div>

      <div class="col-xs-4">
        <img class="img-logos-size" src="{{asset('img/Carrusel/grupo-calidra.png')}}" alt="">
      </div>

   <!--  <div class="item">
      <div class="col-xs-4">
        <img class="img-logos-size" src="{{asset('img/Carrusel/p-g.png')}}" alt="">
      </div>
      <div class="col-xs-4">
        <img class="img-logos-size" src="" alt="">
      </div>
      <div class="col-xs-4">
        <img class="img-logos-size" src="" alt="">
      </div>
    </div> -->

  </div>


</div>


<!-- SECCION DE BANNER PARA MÓVL -->
<div id="my-pics2" class="carousel slide div-logos visible-xs" data-ride="carousel">
  <div class="carousel-inner" role="listbox">

    <div class="item active col-xs-12">
        <img class="img-logos-size" src="{{asset('img/Carrusel/vw.png')}}" alt="">
    </div>

    <div class="item col-xs-12">
      <img class="img-logos-size" src="{{asset('img/Carrusel/bimbo.png')}}" alt="">
    </div>
    <div class="item col-xs-12">
      <img class="img-logos-size" src="{{asset('img/Carrusel/alen.png')}}" alt="">
    </div>

    <div class="item col-xs-12">
      <img class="img-logos-size" src="{{asset('img/Carrusel/cocacola.png')}}" alt="">
    </div>
    <div class="item col-xs-12">
      <img class="img-logos-size" src="{{asset('img/Carrusel/pepsi.png')}}" alt="">
    </div>
    <div class="item col-xs-12">
      <img class="img-logos-size" src="{{asset('img/Carrusel/cryoinfra.png')}}" alt="">
    </div>

    <div class="item col-xs-12">
      <img class="img-logos-size" src="{{asset('img/Carrusel/grupo-calidra.png')}}" alt="">
    </div>

   <!--  <div class="item">
      <div class="col-xs-4">
        <img class="img-logos-size" src="{{asset('img/Carrusel/p-g.png')}}" alt="">
      </div>
      <div class="col-xs-4">
        <img class="img-logos-size" src="" alt="">
      </div>
      <div class="col-xs-4">
        <img class="img-logos-size" src="" alt="">
      </div>
    </div> -->

  </div>

  <a class="left carousel-control" href="#my-pics2" role="button" data-slide="prev">
    <span class="icon-prev" aria-hidden="true"></span>
  </a>
  <a class="right carousel-control" href="#my-pics2" role="button" data-slide="next">
    <span class="icon-next" aria-hidden="true"></span>
  </a>
</div>

</section>
