
<div class="div-banner img-fluid">
    @include('template/layouts/menu')


    <div class="text_banner">
        <p class="texto_banner">{{__('auth.text_banner1')}}</p>
        <h1 class="texto_amarillo">{{__('auth.text_banner2')}}</h1>
        <p class="texto_amarillo">{{__('auth.text_banner3')}}</p>
        <p class="texto_banner">{{__('auth.text_banner4')}}</p>
        <p class="texto_banner">{{__('auth.text_banner5')}}</p>
    </div>
</div>
