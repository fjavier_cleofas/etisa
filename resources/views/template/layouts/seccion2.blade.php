<section class="container-fluid seccion1" id="servicios">
    <div class="rectangulo-servicios col-xs-12 col-md-12 col-sm-12 container-fluid">
      <p class="texto_rectangulo-servicios">{{__('auth.text_hovertit')}}</p>
    </div>

    <div class="no-padding img-fluid img-1 col-md-4 col-xs-12 col-sm-6 ">
      <div class="rectangulo-2  ">
        <p  class="texto_hover top">{{__('auth.text_hover1')}}</p>
        <p  class="texto_hover">{{__('auth.text_hover2')}}</p>
        <p  class="texto_hover">{{__('auth.text_hover3')}}</p>
        <br>
        <p class="texto_hover texto-servicios">{{__('auth.text_hover_hide1')}}</p>
      </div>
    </div>

    <div class="no-padding img-fluid img-2 col-md-4 col-xs-12 col-sm-6">
      <div class="rectangulo-2 ">
        <p class="texto_hover top">{{__('auth.text_hover1')}}</p>
        <p class="texto_hover">{{__('auth.text_hover4')}}</p>
        <p class="texto_hover">{{__('auth.text_hover3')}}</p>
        <br>
        <p class="texto_hover texto-servicios">{{__('auth.text_hover_hide2')}}</p>
      </div>
    </div>

    <div class="no-padding img-fluid img-4 col-md-4 col-xs-6 col-sm-12 ">
      <div class="rectangulo-2 ">
        <p class="texto_hover top1">{{__('auth.text_hover1')}}</p>
        <p class="texto_hover">{{__('auth.text_hover7')}}</p>
        <br>
        <p class="texto_hover texto-servicios">{{__('auth.text_hover_hide3')}}</p>
      </div>
    </div>

</section>

<script>


</script>
