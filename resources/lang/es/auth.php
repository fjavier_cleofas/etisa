<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    /*Pagina de incio*/
    /*Menu*/
  'menu_inicio' => 'Inicio',
  'menu_nosotros' => 'Nosotros',
  'menu_servicios' => 'Servicios',
  'menu_materiales' => 'Materiales',
  'menu_contacto' => 'Contacto',
    /*Banner*/
    'text_banner1' => 'Más de dos décadas',
    'text_banner2' => 'Conservando la energía',
    'text_banner3' => 'térmica industrial',
    'text_banner4' => 'gracias a nuestros materiales',
    'text_banner5' => 'y acabados de calidad',


    /*Banner Expertos*/
    'text_banner6' => 'Expertos en',
    'text_banner7' => 'control de energía',
    'text_banner8' => 'térmica y acústica',
    'text_banner9' => 'Nos dedicamos a la conservación de la' ,
    'text_banner10' => 'energía a nivel industrial, la cuál se presenta,',
    'text_banner11' => 'en su mayor parte, en forma de calor o frío. ',
    'text_banner12' => 'Esta energía es controlada por medio de ',
    'text_banner13' => 'materiales y acabados de calidad,',
    'text_banner14' => 'ecologizando el proceso y haciéndolo mucho',
    'text_banner15' => 'más rentable para el usuario y su entorno.',

    /* Textos Menu Hover */

    'text_hover1' => 'Aislamiento',
    'text_hover2' => 'térmico de alta',
    'text_hover3' => 'temperatura',
    'text_hover4' => 'térmico de baja',
    'text_hover5' => 'térmico',
    'text_hover6' => 'refractario',
    'text_hover7' => 'Acústico',

    'text_hover_hide1' => 'Aplicación de materiales con excelente factor de retención de energía para equipos e instalaciones con temperatura modificada por encima de la temperatura ambiente. Referente a materiales aplicables en áreas internas  de hornos, calderas, etc. Cuya temperatura de operación oscila entre los 800°C y hasta 1400°C.',

    'text_hover_hide2' => 'Relativo a materiales termoaislantes caracterizados por operar instalaciones o equipos cuya temperatura de funcionamiento es inferior a la del medio ambiente y hasta -160° C.',

    'text_hover_hide3' => 'Se refiere a la propagación de ruido de un local a otro o de un local hacia el exterior. Aplicación de materiales con excelente factor de absorción o de correción acústica de 45dBA a 150dBA.',


    'text_hover_hidetit1' => 'Aislamiento térmico',
    'text_hover_hidetit2' => 'de alta temperatura',
    'text_hover_hidetit3' => 'de baja temperatura',
    'text_hover_hidetit4' => 'Aislamiento',
    'text_hover_hidetit5' => 'térmico refractario',
    'text_hover_hidetit6' => 'Acústico',

    'text_hovertit' => 'Servicios de aislamiento',

    /* Texto Seccion 3*/

    'text_sec3' => 'Nuestros Clientes',

    /*Footer*/
    'text_footer1' => '3 Sur #10520-A Col. Loma Encantada. Puebla, Pue. C.P. 72474',
    'text_footer2' => 'Tel. 222 (755-47-48) - 222 (755-47-49)',
    'text_footer3' => 'Móvil 2226703707',
    'text_footer4' => 'ventas@etisa.mx',
    'text_footer5' => 'etisa@etisa.mx',
    /*Nostros*/
    'text_us_banner1' => 'Somos una empresa',
    'text_us_banner2' => 'con cobertura nacional',
    'text_us_banner3' => 'Reducimos sustancialmente los costos de producción de nuestros',
    'text_us_banner4' => 'clientes, estimulando positivamente su crecimiento.',
    'text_us_banner5' => 'Todo esto sin olvidarnos del medio ambiente.',

    'text_us_section1' => 'ETISA es una empresa comprometida con una visión de común crecimiento. Nuestros asociados, proveedores y trayectoria apegada a los valores, nos dan una ventaja competitiva en el logro del éxito común.',
    'text_us_section2' => 'Apoyamos a cualquier rama de la industria, en cuyo proceso se emplee alguna forma de energía térmica o acústica; de igual manera aportamos confort a sus líneas de producción a través del control de sus emisiones de ruido.',

    'text_carousel_us1' => 'Calidad',
    'text_carousel_us2' => 'La calidad nunca es un accidente; siempre es el resultado de un esfuerzo de la inteligencia. En ETISA buscamos hacer las cosas bien, a la primera.',

    'text_carousel_us3' => 'Medio ambiente',
    'text_carousel_us4' => 'Salvaguardar el medio ambiente debe ser un compromiso humano. Creemos que vale la pena, vivir en este mundo para hacerlo mejor.',

    'text_carousel_us5' => 'Salud',
    'text_carousel_us6' => 'En toda sociedad el respeto a los demás comienza con el respeto hacia nuestra persona. Creemos que la primera riqueza es la salud, con ella el crecimiento esta garantizado.',

    'text_carousel_us7' => 'Seguridad',
    'text_carousel_us8' => 'Todos los procedimientos de seguridad tienen una razón de existir. Estamos comprometidos con la integridad absoluta de nuestra herramienta humana, que hace la diferencia en nuestro trabajo.',

    'text_carousel_us9' => 'NUESTRA MISIÓN',
    'text_carousel_us10' => 'Satisfacer integralmente a nuestros clientes a través de la calidad total de los productos y servicios que ofrecemos. Estamos comprometidos con atender todas sus necesidades en el control de Energía Térmica y Acústica.',

    'text_carousel_us11' => 'NUESTRA VISIÓN',
    'text_carousel_us12' => 'Ser una empresa consolidada en el mercado nacional, innovadora, ordenada, que ostente la confianza de sus clientes. Líder en el mercado del aislamiento térmico con personal calificado y dispuesto a enfrentar los retos que impone el servicio que ofrecemos.',
    'text_carousel_us13' => 'Su razón la satisfacción integral de las necesidades de control de energía de la industria mediante el diseño y aplicación de materiales termo aislantes empeñados en la búsqueda de la calidad total.',

    'text_carousel_us14' => 'VALORES',
    'text_carousel_us15' => 'Texto pendiente',


    /*Texto Contacto*/
    'text_contacto' => 'Queremos crecer',
    'text_contacto1' => 'junto contigo',
    'text_contacto2' => 'Etisa es una empresa comprometida con una',
    'text_contacto3' => 'visión de común crecimiento. Nuestros asociados' ,
    'text_contacto4' => 'proveedores y trayectoria apegada a los valores,',
    'text_contacto5' => 'nos dan una ventaja competitiva en ',
    'text_contacto6' => 'el logro del éxito común.',
    'text_contacto7' => 'Reducimos sustancialmente los costos de ',
    'text_contacto8' => 'producción de nuestros clientes, estimulando ',
    'text_contacto9' => 'positivamente su crecimiento.',

];
