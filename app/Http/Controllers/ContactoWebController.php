<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ContactoWebController extends Controller
{
  public function contacto(Request $request) {

    \Mail::raw($request->message, function ($message) use($request){
      $asunto = $request->subject;
      $nombre = $request->name;
      $generated = 'Asunto: ' . $asunto . '<br>' .
                   'Nombre: ' . $nombre . '<br>' .
                   'Email: ' . $request->email . '<br>' .
                   'Mensaje: ' . $request->message . '<br>';
      $message->from($request->email, $request->subject);
      $message->to('etisa@etisa.mx')
              ->subject($request->subject)
              ->setBody($generated, 'text/html');;
    });


    flash('Se envió su mensaje de forma adecuada.')->success();
    return redirect('/');
  }
}
